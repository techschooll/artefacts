mkdir TechSchool
cd TechSchool
git clone https://gitlab.com/techschooll/user-service.git
git clone https://gitlab.com/techschooll/gateway-service.git
git clone https://gitlab.com/techschooll/course-service.git

cd gateway-service
rm -rf node_modules
npm i 
cd ../course-service
rm -rf node_modules
npm i
cd ../user-service
rm -rf node_modules
npm i 
npm i json-server --g
node app.js & node ../gateway-service/app.js & node ../course-service/app.js & json-server --watch ./data-access/db/user-roles.json & json-server --watch ../course-service/data-access/db/course.json --port 4000

