#TechSchool

For postman collection - Please refer attached collection file (TechSchool.postman_collection.json)

For postman environment - Please refer attached environment file (Techschool.postman_environment.json) - Please ensure Techschool is the selected environment, before executing the API's in postman

**Setup**

  1. Please ensure the following versions of node (version v12.16.1) and npm are installed in your local system. To switch between node version, please use 'n' module (https://www.npmjs.com/package/n)

  2. Once the above step is done, please clone this repo and in the artefacts folder, please execute the below command - 

> bash setup.sh

  This would install 3 microservices namely, user, gateway and course and also spawn json db servers for user and course services. Detailed explanation of each microservice is given below

**Architecture**

  User management and course management are the two main components in concern. I decided to build two separate micro-services for user management and course management. The main advantage with this approach is that, we can scale these services separately based on the load and also testing / development / deployment of these services can happen separately. In addition, I have built a gateway service as well which would be the only service exposed to the outside network. Gateway service would proxy the requests to respective services. The advantage of having a gateway service is not expose our internal infra to any attacks like ddos and also to have any common logic such as rate limiting / ssl termination in the gateway service layer itself. In order to spawn multiple microservices, the core logic of spawning a server / common helpers / data accessors, I have built a separate lib - hapi-server-lib which could be referenced in micro-services and services could be easily started.

**  Microservice architecture**

    The microservices are built with node JS with hapi framework (version 18) - https://hapi.dev/ . The external libraries referenced across the services are 
    1. hapi - node framework to build services
    2. joi - to handle schema validations for payload, params, query params, response
    3. jsonwebtoken - to handle jwt's for authorization
    4. axios - to make https requests and handle promises
    5. hapi-swaggered, hapi-swaggered-ui, vision, inert - to enable swagger for the routes
    6. h2o2 - to enable proxy
    7. json-server - to create mock db servers
    8. mocha and chai - assertion and test framework
    9. nyc - to create coverage report

**Assumptions** 

  1. User authentication is already taken care. Authorization is handled with the help of jwt's
  2. Course is a collection of section and section is a collection of lessons
  3. Lesson could have contents and multiple authors could work on their copy of  lessons  
  4. An author can merge the lesson only to the master version. If for some reaso n,the mas ter version  is changed after the author has forked his branch, then we would check for conflicts and throw an error with the conflict keys  
  5. User can manually resolve the conflict and update the same with  the API  
  6. Only after all draft versions are merged, the master version can  be saved  
  7. Only after a master version is saved, it can be published  
  8. Only published lessons, could be part of sections. You cannot add lesson to a  section sequence if its not published
  9. One or more sections could be added to a course 
  10. Course / Section sequences and contents could be edited by the authors
  11. There are three roles in the system - admin, author and user and API's are restricted based on the role of the user who is trying to do the action

**Code Structure**

  ![](code-structure.png)

**Microservices and Data Model**

**1. Gateway Service**

  Gateway service acts as a proxy service and also has common logic like adding requestId's to each request. This service doesnt hold any routes as such and it does redirects the request to downstream services. Info (host and port to redirect) about the downstream services are maintained in its config. All API's are routed to localhost:3200 in this case (gateway service port) and the actual service redirection happens in gateway

**2. User Service**
  
  Swagger - http://localhost:3400/documentation#/v1 (Please ensure service is started before you hit this)

  User service is responsible for user management. Users, Roles and UserRoles are the 3 main entity of user service. An user could be part of multiple roles. Once a user logs in, getjwt API is called, which would return a jwt signed with a secret key. The jwt has an expiry of 1 hour and it contains the list of roles that the user is eligible for. This JWT is passed in the x-header (could be maintained in the client in a cookie) and each route (in all microservices), validates if the user is authorized for the route with the jwt information. This enables authorization at the route layer itself. The jwt is then decoded and passed in an header (x-internal-credentials) to the services and this could be used to understand the details of the user who is accessing the route. The user service has 7 API's to getjwt, create user, fetchroles, update user, update user role, remove user role and remove user

  To run the tests (ensure db json server is running), execute the following at the root of user service

  npm test

  Code Coverage - The current line coverage of user service is 90.55%

  ![](user-service.png)

**2. Course Service**

  Swagger - http://localhost:3600/documentation#/v1 (Please ensure service is started before you hit this)

  Course service contains five major entities - Courses, Sections, Lessons, LessonVersions, LessonHistory. Authors (authorization handled with jwt in the route layer) could fork lessons which are created / merged and each author's copy is persisted in LessonVersions table to which authors could make multiple updates. Each forked version also contains a base version. When an author is ready to merge the changes to master, we check if the author's base version and the master version are the same. If they are same, we merge the author's version to master and increment the version. Every time an update happens to lesson table, we maintain the previous snapshot in the LessonHistory table. So when an author is trying to merge his version to master and if the master version and author's base version is not same (master has changed after author has forked his version), then we identify the conflicting keys between author's base version (fetched from LessonHistory) and current master and report the conflicts. The author can manually correct the conflicts and merge to master version there-after. When a lesson is updated to saved stated, a check is made to ensure all draft versions are merged. Similarly only saved versions could be published. Check is added in place to ensure only published lessons are added to sections and only published courses are fetched in the fetch all courses API. Course service has 10 API's to create / update course, create section, create lesson, fork a lesson, merge lesson, resolve conflicts in lesson, update lesson, change lesson status, update section, fetch all courses

  To run the tests (ensure db json server is running), execute the following at the root of user service

  npm test

  Code Coverage - The current line coverage of course service is 83.51%

  ![](course-service.png)

**2. Hapi-Server-Lib**

  Hapi Server Lib acts as the base lib and contains base app loader which helps in regestering dependencies and spawning a server. It also contains base adapter files which provides wrapper methods over axios for crud opertaions. It also has an helper method to authenticate the requests (with jwt). 


**Improvements**

1. Using redis to cache frequently access data
2. Logging with ELK and integrating with some alerting system to catch error
3. Move secret keys to vault
4. If state transitions can be handled implicitly, we can either use state machines to handle them. We can invoke the state machines after every update to a lesson version. Alternatively, we could handle this asynchronously with rabbitmq and a rule engine as well
5. Automate unit test and coverage report as part of git pre-commit hook to catch failures during commit
